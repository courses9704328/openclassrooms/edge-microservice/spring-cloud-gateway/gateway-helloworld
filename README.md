This is a course about **Spring Cloud Gateway**:

This repository has a number of branches , each of them deals with different SpringCloudGateway-related topic as follows:

* Define routes with properties configuration: **gateway-route-property-config**
* Define routes with java configuration: **gateway-routes-java-config**
* Define a filter with properties configuration: **gateway-filters-properties-config**
* Define a filter with java configuration: **gateway-filters-java-config**
* Define a global filter with javaonfiguration: **gateway-global-filter-java-config**
* Define a custom filter with javaonfiguration: **gateway-custom-filter-java-config**
